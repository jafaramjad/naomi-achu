import { createClient } from 'contentful'

const API_CONFIG = {
    spaceId: 'bu8vnicgwfzk',
    accessToken: 'ZENzL3HMeoEiNgIc-4KYkFqT2SIXKtmekUuk6cq6zp8'
}


class Contentful {
    constructor() {

        this.authorized = ''
        this.client = ''
        const contentfulApp = this.initClient(API_CONFIG);



        // console.log(contentfulApp)

        this.providers = {
            client: contentfulApp
        };

    }

    // INIT CLIENT
    initClient(config) {

        this.client = createClient({
            space: config.spaceId,
            accessToken: config.accessToken,
            host: 'cdn.contentful.com'
        })

        return this.client.getSpace()
            .then((space) => {
                this.authorized = true
                return space
            })
    }

    loadData(contentTypeId) {

        return this.client.getEntries({
            content_type: contentTypeId
        }).then(payload => {
            return payload.items
        })
    }

    loadTestData(contentTypeId) {

        // console.log(contentTypeId)
        // console.log(this)

        return this.client.getEntries({
            content_type: contentTypeId
        }).then(payload => {
            return payload.items
        })
    }

    // END CLASS
}


export default Contentful;
