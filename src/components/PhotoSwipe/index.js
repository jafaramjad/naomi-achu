import React from 'react'
import { Gallery, Item } from 'react-photoswipe-gallery'

import 'photoswipe/dist/photoswipe.css'
import 'photoswipe/dist/default-skin/default-skin.css'

import './styles.css'

const PhotoSwipe = (props) => {


    return (
        <div className="gallery-shell">

            <Gallery>
                {
                    props.data && props.data.reverse().map((itm, idx) => {
                        return (
                            <div className="gallery-item" key={`gallery-item-${idx}`}>
                                <Item

                                    original={`${itm.fields.image.fields.file.url}`}
                                    thumbnail={`${itm.fields.image.fields.file.url}?w=250&h=250`}
                                    width={`${itm.fields.image.fields.file.details.image.width}`}
                                    height={`${itm.fields.image.fields.file.details.image.height}`}
                                >
                                    {({ ref, open }) => (
                                        <img alt={`${itm.fields.title}`} ref={ref} onClick={open} src={`${itm.fields.image.fields.file.url}?w=450&h=450`} />
                                    )}
                                </Item>
                            </div>
                        )
                    })
                }

            </Gallery>
        </div>
    )
}

export { PhotoSwipe as default }