import React from 'react'

import './styles.css'


const SoundCloudPlayer = () => {

    // STATE
    const [isOpen, setIsOpen] = React.useState(false)


    // React.useEffect(() => {
    // }, [])

    const iframeLoaded = () => {
        // const iframeEl = document.getElementById('soundcloudIframe')
        // const docEl = iframeEl.contentWindow //.getElementsByClassName('sc-link-dark')


        // console.log(iframeEl)
        // console.log(docEl)

        // let newStyles = `<style>
        //     .sc-link-dark {color: #fff;}
        // </style>`

        // docEl.body.innerHTML = docEl.body.innerHTML + newStyles;

        // linkEl.style.color = '#ffffff'
    }

    return (
        <div className={`musicplayer ${isOpen ? 'is-open' : 'is-closed'}`}>

            <div>
                {
                    isOpen
                        ? (
                            <div className={'togglebtn'} onClick={() => setIsOpen(false)}>^</div>
                        )
                        : (
                            <div className={'togglebtn'} onClick={() => setIsOpen(true)}>^</div>
                        )
                }
            </div>

            {/* <iframe src="https://audiomack.com/embed/playlist/audiomack-africa/verified-afrobeats" scrolling="no" width="100%" height="400" scrollbars="no" frameborder="0"></iframe> */}
            {/* <iframe
                id="soundcloudIframe"
                title="Naomi Achu - Sound Cloud Player"
                width="100%"
                height={isOpen ? 450 : 20}
                scrolling="no"
                onLoad={() => iframeLoaded()}
                frameBorder="no"
                allow="autoplay"
                src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/268542677&color=%232a1610&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"
            ></iframe> */}

            <iframe
                id="soundcloudIframe"
                title="Naomi Achu - Sound Cloud Player"
                width="100%"
                height={isOpen ? 450 : 20}
                scrolling="no"
                onLoad={() => iframeLoaded()}
                frameborder="no"
                allow="autoplay"
                src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/831037354&color=%232a1610&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true">
            </iframe>

            {/* <div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/naomiachuofficial" title="Naomi Achu" target="_blank" style="color: #cccccc; text-decoration: none;">Naomi Achu</a> · <a href="https://soundcloud.com/naomiachuofficial/robo-robo" title="Robo Robo" target="_blank" style="color: #cccccc; text-decoration: none;">Robo Robo</a></div> */}


        </div>
    )
}

export { SoundCloudPlayer as default }