import React from 'react'
import YoutubeBackground from 'react-youtube-background'


export default YouTubeBG = () => {

    return (
        <YoutubeBackground
            videoId={'dV7TDzTlAWo'}                /* default -> null */
            aspectRatio={"16:9"}            /* default -> "16:9" */
            //   overlay={string}                /* default -> null | e.g. "rgba(0,0,0,.4)" */
            className={styles.homepageVideo}              /* default -> null */
            //   nocookie={bool}                 /* default -> false | sets host to https:/*www.youtube-nocookie.com to avoid loading Google's cookies */
            playerOptions={{
                start: 14,
                end: 300
            }}          /* default -> {}  | https://developers.google.com/youtube/player_parameters*/
        //   onReady={func}                  /* default -> null | returns event with player object */
        //   onEnd={func}                    /* default -> null | returns event with player object */
        //   onPlay={func}                   /* default -> null | returns event with player object */
        //   onPause={func}                  /* default -> null | returns event with player object */
        //   onError={func}                  /* default -> null | returns event with player object */
        //   onStateChange={func}            /* default -> null | returns event with player object */
        //   onPlaybackRateChange={func}     /* default -> null | returns event with player object */
        //   onPlaybackQualityChange={func}  /* default -> null | returns event with player object */

        >
            {/* YOUR CONTENT */}
        </YoutubeBackground>
    )
}