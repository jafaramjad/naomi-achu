import React from 'react'
import { Parallax } from 'react-parallax';

// import {
//     Button,
//     // Container,
//     // Card,
//     // CardContent,
//     // CardMedia,
//     Typography,
//     Grid
// } from '@material-ui/core';

import styles from './styles.module.css'
// import SocialLinks from '../../../../components/SocialLinks'
// import bgImg from './images/bg1.jpg'


export default function Hero(props) {


    return (
        <div className={styles.hero} style={{ backgroundColor: `${props.bgColor}` }}>
            <Parallax>
                {props.children}
            </Parallax>
        </div>

    )
}