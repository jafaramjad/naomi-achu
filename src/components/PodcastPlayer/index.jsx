import React from 'react'
import PlayCircleOutlineIcon from '@material-ui/icons/PlayCircleOutline';
const PodcastPlayer = (props) => {

    const [display, setDisplay] = React.useState(false)


    return (
        <div>
            {
                display
                    ? (
                        <iframe
                            title={props.title}
                            src={props.src}
                            height="102px"
                            width="100%"
                            frameBorder="0"
                            scrolling="no"></iframe>
                    )
                    : (
                        <div>
                            <PlayCircleOutlineIcon
                                style={{ fontSize: 64, cursor: 'pointer' }}
                                onClick={() => setDisplay(!display)}
                            />
                        </div>
                    )
            }


        </div>
    )
}

export { PodcastPlayer as default }