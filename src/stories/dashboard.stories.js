import React from 'react';

import { Page } from '../routes/home/components/Page';

export default {
    title: 'Homepage/Hero',
    component: Page,
};

const Template = args => <Page {...args} />;

export const Default = Template.bind({});
Default.args = {
    task: {
        id: '1'
    },
};