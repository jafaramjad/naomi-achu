import React from 'react'
import PhotoSwipe from 'components/PhotoSwipe'

// import { documentToHtmlString } from '@contentful/rich-text-html-renderer';
// import { richTextFromMarkdown } from '@contentful/rich-text-from-markdown'

import './styles.css'

// import adLawNew from './images/ads/law-new2.jpg'

export const Page = (props) => {

	/* const [htmlString, setHTMLString] = React.useState([])

	let nodeLinks = node => {
		if (node.type === 'image') {
			return `<img src='${node.url}' alt='${node.alt}' title='${node.title}' />`
		}
	}

	React.useEffect(() => {

		getHtmlString()

		// eslint-disable-next-line 
	}, [])

	const getHtmlString = async () => {
		let tmpRichText = await richTextFromMarkdown(props.data[0].fields.body, nodeLinks)
		// console.log(tmpRichText)
		// setHTMLString(tmpRichText.content.toString())
		setHTMLString(tmpRichText.content)
	}
 */


	return (
		<div className="page-content gallery">
			<h2>
				Gallery
			</h2>

			<PhotoSwipe data={props.data} />


			{
				// htmlString.map((itm, idx) => {
				// 	return (
				// 		<div
				// 			key={`gallery-img-${idx}`}
				// 			className="img-shell"
				// 			dangerouslySetInnerHTML={{ __html: itm }}
				// 		/>
				// 	)
				// })
			}


		</div>
	)
}

// export { Page as default }