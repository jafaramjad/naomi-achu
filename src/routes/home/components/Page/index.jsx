import React from 'react'
import PhotoSwipe from 'components/PhotoSwipe'
// import {
// 	Button,
// 	Box,
// 	// Container,
// 	// Card,
// 	// CardContent,
// 	// CardMedia,
// 	Typography,
// 	Grid
// } from '@material-ui/core'
// import { Link } from 'react-navi'
// import Hero from '../Hero'
// // import PodcastsRecent from '../PodcastsRecent'
// import Offers from '../Offers'
// import { Background } from 'react-parallax'

// import styles from './styles.module.css'
import './styles.css'

// import shopBanner from './images/Naomi_Shop_Banner.jpg'

import bgLogo from './images/AfricanPepper/logo_african_pepper.png'
import imgNaomiAlbumFront from './images/AfricanPepper/naomi_album_front.png'


export function Page(props) {
	return (
		<div>


			<div id="heroFrontPage">


				<img
					id="heroAlbum"
					src={imgNaomiAlbumFront}
					alt={'Naomi African Pepper'}
				/>

				<div className="heroBGRed"></div>
				<div className="heroBGBlack"></div>

				<div className="heroBG"></div>

				<div className="heroListen">
					<img
						id="heroLogo"
						className={'heroBackgroundImg'}
						src={bgLogo}
						alt={'Naomi Achu African Pepper'}
					/>


					<a href="https://direct.me/naomiachu" rel="noopener noreferrer" target="_blank">Listen Now</a>
				</div>
			</div>

			<div className="heroVideo">
				<iframe
					src="https://www.youtube.com/embed/eNfIRcc0TpA"
					title="YouTube video player"
					frameborder="0"
					allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
					allowfullscreen></iframe>
			</div>

			<div className='heroGallery'>
				<h3 className='section-title'>Gallery</h3>

				<PhotoSwipe data={props.data} />
			</div>

		</div>
	)
}
