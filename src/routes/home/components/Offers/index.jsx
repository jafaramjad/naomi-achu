import styles from './index.module.css'
import React from 'react'
import {
    Box,
    Grid,
    Container,
    // Card,
    // CardContent,
    // CardMedia,
    Typography

} from '@material-ui/core';


import badgeAppleMusicImg from './images/US-UK_Apple_Music_Badge_RGB.svg'
import albumRoboRobo from './images/Album-RoboRobo.jpg'
// import bgImg from './images/Naomi_JungleJuice.jpg'

export default function Offers(props) {



    return (
        <div className={styles.offersContainerShell}>
            {/* <img src={bgImg} alt="Jungle Juice - Naomi" /> */}

            <div className={styles.offersContainer}>
                <br />
                <Box textAlign="center">
                    <Typography variant="h4">
                        New Album!
                    </Typography>
                    <Typography variant="subtitle1">
                        SUMMER 2020
                    </Typography>

                    <Typography variant="subtitle1">
                        Featuring New Hit Single "Robo Robo"
                        </Typography>
                </Box>
                <br />

                <Box textAlign="center">

                    <Box textAlign="center">

                        <div className={styles.albumShell}>
                            <img src={albumRoboRobo} alt="Listen on Apple Music" />

                        </div>
                    </Box>

                    {/* 
                      <Typography variant="h5">
                        Robo Robo
                        </Typography>
                    <Typography variant="subtitle1">
                        Featuring Naomi's New Hit ""
                        </Typography>
                    <Typography variant="subtitle2">
                        - Track 1
                        - Track 2
                        - Track 3
                        </Typography> */}
                </Box>

                <br />
                <br />
                <Box textAlign="center">
                    <Typography variant="subtitle2">

                        <a href="https://music.apple.com/us/artist/naomi-achu/338652051" target="_blank" rel="noopener noreferrer" >
                            <img src={badgeAppleMusicImg} alt="Listen on Apple Music" />
                        </a>
                        <br />
                    </Typography>
                </Box>

                <br /><br />
            </div>
            <br />
            <br />

        </div>
    )
}