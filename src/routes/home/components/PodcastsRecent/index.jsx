import React from 'react'
import Carousel from 'react-multi-carousel'
import { format } from 'date-fns'
import { parseFromTimeZone } from 'date-fns-timezone'

import {
    Box,
    Grid,
    Container,
    Card,
    CardContent,
    CardMedia,
    Typography

} from '@material-ui/core'

import PodcastPlayer from 'components/PodcastPlayer'
import "react-multi-carousel/lib/styles.css"
import styles from './index.module.css'


export default function PodcastsRecent(props) {

    // GET DATE FUNC
    function getDate(d) {

        let tmpDate = parseFromTimeZone(new Date(d), { timeZone: 'America/New_York' }) // , 'YYYY-MM-DD', { timeZone: 'America/New_York' }
        let tmpDate2 = format(tmpDate, 'MMM do, yyyy')

        return tmpDate2
    }

    const responsive = {
        superLargeDesktop: {
            // the naming can be any, depends on you.
            breakpoint: { max: 4000, min: 3000 },
            items: 5,
        },
        desktop: {
            breakpoint: { max: 3000, min: 1024 },
            items: 3,
        },
        tablet: {
            breakpoint: { max: 1024, min: 464 },
            items: 2,
        },
        mobile: {
            breakpoint: { max: 464, min: 0 },
            items: 1,
        },
    }

    return (
        <div className={styles.shellContainer}>

            <Grid container>
                <Grid item xs={12} className={styles.podcastShell}>

                    <Container>
                        <Box textAlign="center">
                            <Typography variant="h4">Our Recent Shows</Typography>
                        </Box>
                        {/* <Grid container spacing={2}> */}
                        <Carousel
                            responsive={responsive}
                            slidesToSlide={1}
                            showDots
                            infinite
                        >

                            {
                                props.data.map(itm => {


                                    let anchorEpisodeNum = itm.fields.anchor.split('episodes/')
                                    let anchorEmbed = anchorEpisodeNum[1] ? `https://anchor.fm/franchised/embed/episodes/${String(anchorEpisodeNum[1])}` : null


                                    return (
                                        // <Grid item xs={12} sm={4} key={itm.sys.id}>

                                        <Card className={styles.podcastCard} key={itm.sys.id}>
                                            <CardContent>

                                                <CardMedia
                                                    component="img"
                                                    className={styles.podcastFlyer}
                                                    src={itm.fields.bannerImg && itm.fields.bannerImg.fields
                                                        ? itm.fields.bannerImg.fields.file.url
                                                        : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mPU0dGtBwACFwEGOu2rcAAAAABJRU5ErkJggg=="
                                                    }
                                                    title={itm.fields.bannerImg && itm.fields.bannerImg.fields
                                                        ? itm.fields.bannerImg.fields.title
                                                        : "Flyer Coming Soon"
                                                    }
                                                />


                                                <Typography component="h5" variant="h5">
                                                    Episode {itm.fields.episode}: {itm.fields.title}
                                                </Typography>
                                                <Typography variant="h6" color="textSecondary">
                                                    {itm.fields.date ? getDate(itm.fields.date) : 'TBD'}
                                                </Typography>

                                                {anchorEmbed ? (

                                                    <PodcastPlayer
                                                        title={itm.fields.title}
                                                        src={anchorEmbed} />
                                                ) : (
                                                        <Typography variant="subtitle1" >Coming Soon...</Typography>
                                                    )}
                                                {/*  <Typography variant="subtitle2" color="textSecondary">
                                                        {itm.fields.subTitle}
                                                    </Typography> */}

                                            </CardContent>

                                        </Card>
                                        /* </Grid> */

                                    )
                                })
                            }
                        </Carousel>

                        {/* </Grid> */}
                    </Container>
                </Grid>
            </Grid>
        </div>
    )
}