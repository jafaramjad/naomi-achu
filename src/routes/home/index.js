import React from 'react'
import { map, route } from 'navi'
// import { format } from 'date-fns'
// import { parseFromTimeZone } from 'date-fns-timezone'

// import Contentful from '../../utils/contentful'
// const C = new Contentful()

export default map(async (req, ctx) => {

  //  console.log(ctx)
  // ctx.providers.client().then(resp => {
  //   console.log('woaaa...')
  // })

  return await route({
    title: "Naomi Achu - Queen of Bamenda",
    getView: async (req, context) => {

      //  console.log(context)

      const { Page } = await import('./components/Page')

      return await context.loadData && context.loadData('gallery').then(resp => {

        // // SORT DATA BY EPISODE
        // let sortedByEpisode = resp.sort((a, b) => {
        //   return a.fields.episode - b.fields.episode
        // })

        // let sliceData = sortedByEpisode.reverse().filter(itm => {
        //   return itm.fields.anchor
        // })

        console.log('home: ', resp)

        return <Page data={resp} />

      })

    },
  })


  /*   return ctx.authorized
  
      ? route({
        title: "Franchised: Shane Douglas & Brian Reznor",
        getView: async (req, context) => {
  
          console.log(context)
  
          const { Page } = await import('./components/Page')
  
          return await ctx.loadTestData('podcast').then(resp => {
  
            // SORT DATA BY EPISODE
            let sortedData = resp.sort((a, b) => {
              return a.fields.episode - b.fields.episode
            })
            return <Page data={sortedData} />
  
          })
  
        },
      })
      : route({}) // BLANK */
})




/* export default route({
  title: "Shane Douglas Podcast",
  getView: async (req, context) => {

    // const { Home } = await import('./Home')
    const { Page } = await import('./components/Page')

    return await C.loadTestData('podcast').then(resp => {

      // console.log(resp)

      // SORT DATA BY EPISODE
      let sortedData = resp.sort((a, b) => {
        return a.fields.episode - b.fields.episode
      })

      return <Page data={sortedData} />

    })

    // const { Home } = await import('./Home')
    // return <Home data={data} />
    // import('./document.mdx')
  },
}) */