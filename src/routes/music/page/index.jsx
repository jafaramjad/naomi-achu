import React from 'react'

import styles from './styles.module.css'

// import adLawNew from './images/ads/law-new2.jpg'
// one-time init of the embedly platform.js, hidden behind 
// typeof check not to blow up on server-side render
if (typeof window !== 'undefined') {
	let init_embedly = function (w, d) {
		var id = 'embedly-platform', n = 'script';
		if (!d.getElementById(id)) {
			w.embedly = w.embedly || function () { (w.embedly.q = w.embedly.q || []).push(arguments); };
			var e = d.createElement(n); e.id = id; e.async = 1;
			e.src = ('https:' === document.location.protocol ? 'https' : 'http') + '://cdn.embedly.com/widgets/platform.js';
			var s = d.getElementsByTagName(n)[0];
			s.parentNode.insertBefore(e, s);
		}
	}
	init_embedly(window, document)
}

export function Page(props) {
	return (
		<div className={styles.about}>
			<h2>
				{props.data[0].fields.title}
			</h2>



			<div dangerouslySetInnerHTML={{ __html: props.data[0].fields.body }} />

			{/* <script

				src="//cdn.embedly.com/widgets/platform.js"
				charset="UTF-8"></script> */}



		</div>
	)
}