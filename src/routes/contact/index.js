import React from 'react'
import { map, route } from 'navi'

// import * as Navi from 'navi'
// export default Navi.route({
//   title: "About",
//   getView: () => import('./document.mdx'),
// })

const PAGE_ID = "Hlz8VG79NmZAyNPw5FKUf"

export default map(async (req, ctx) => {

  return await route({
    title: "Contact Naomi",
    getView: async (req, context) => {

      //  console.log(context)

      const { Page } = await import('./page')

      return await context.loadData && context.loadData('static').then(resp => {

        // console.log(resp)

        const tmpData = resp.filter(itm => {
          return itm.sys.id === PAGE_ID
        })

        return <Page data={tmpData} />

      })

    },
  })

})
