import React from 'react'
import { map, route } from 'navi'

// import * as Navi from 'navi'
// export default Navi.route({
//   title: "About",
//   getView: () => import('./document.mdx'),
// })

// const PAGE_ID = "1nnN1V0TSflFS4uiQbZdWn"

export default map(async (req, ctx) => {

  return await route({
    title: "Naomi News",
    getView: async (req, context) => {

      //  console.log(context)

      const { Page } = await import('./page')

      return await context.loadData && context.loadData('news').then(resp => {

        console.log(resp)

        // const tmpData = resp.filter(itm => {
        //   return itm.sys.id === PAGE_ID
        // })

        return <Page data={resp} />

      })

    },
  })

})
