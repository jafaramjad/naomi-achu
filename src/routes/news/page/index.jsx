import React from 'react'

import styles from './styles.module.css'

// import adLawNew from './images/ads/law-new2.jpg'

export function Page(props) {
	return (
		<div className={styles.about}>
			<h2>
				{props.data[0].fields.title}
			</h2>
			<p>
				{props.data[0].fields.body}
			</p>


		</div>
	)
}