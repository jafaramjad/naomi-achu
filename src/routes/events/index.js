import React from 'react'
import { map, route } from 'navi'

// WIDGET
// https://www.artists.bandsintown.com/support/widget-installation


// import * as Navi from 'navi'
// export default Navi.route({
//   title: "About",
//   getView: () => import('./document.mdx'),
// })

const PAGE_ID = "O4ZzOa8ZlSPavpPgmcP6G"

export default map(async (req, ctx) => {

  return await route({
    title: "Events",
    getView: async (req, context) => {

      //  console.log(context)

      const { Page } = await import('./page')

      return await context.loadData && context.loadData('static').then(resp => {

        console.log(resp)

        const tmpData = resp.filter(itm => {
          return itm.sys.id === PAGE_ID
        })

        return <Page data={tmpData} />

      })

    },
  })

})
