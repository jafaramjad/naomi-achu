import React from 'react'
import format from 'date-fns/format'
import { parseFromTimeZone } from 'date-fns-timezone'
import { Background } from 'react-parallax';

import Hero from 'components/Hero'

import './styles.css'
import bgImg from './images/naomi_home_bg.jpg'



export function Page(props) {

	const [listEvents, setListEvents] = React.useState([])

	const getEvents = async () => {
		//let resp = await fetch('https://rest.bandsintown.com/artists/Sweater%20Beats/events?app_id=Test')
		// let resp = await fetch('https://rest.bandsintown.com/artists/NaomiAchu/events')
		let resp = await fetch('https://rest.bandsintown.com/artists/naomiachuworldwide/events?app_id=Test&date=all')

		let data = await resp.json()

		// console.log(resp)
		// console.log(data)
		setListEvents(data)
	}

	// GET DATE FUNC
	function getDate(dateVal, fmtDate = 'MMM do, yyyy') {
		let tmpDate = parseFromTimeZone(new Date(dateVal), { timeZone: 'America/New_York' })
		let tmpDate2 = format(tmpDate, fmtDate)
		return tmpDate2
	}

	React.useEffect(() => {

		getEvents()

	}, [])

	return (
		<div className={'event-page'}>

			<Hero bgColor={'#ccc'}>
				<Background className="heroBackground">
					<img
						className='heroBackgroundImg'
						src={bgImg}
						alt={'Naomi Achu alley'}
					/>
				</Background>




				<div style={{ minHeight: '455px' }} />

			</Hero>


			<div className={'event-content'}>

				<h2>
					{props.data[0].fields.title}
				</h2>
				<p>
					{props.data[0].fields.body}
				</p>

				{
					listEvents.reverse().map(itm => {
						return (

							<div className='event-box'>
								<div className='event-date'>
									<div>{getDate(itm.datetime, 'MMM')}</div>
									<div className='event-day'>{getDate(itm.datetime, 'dd')}</div>
									<div className='event-year'>{getDate(itm.datetime, 'yyyy')}</div>
								</div>
								<div className='event-info'>
									<div>
										<h3>
											<a target="_blank" rel="noopener noreferrer" href={itm.url}>
												{itm.venue.name}
											</a>
										</h3>
									</div>
									<div className="flex space-between">
										<span>Location: {itm.venue.type === 'Virtual' ? itm.venue.type : `${itm.venue.city}, ${itm.venue.region ? itm.venue.region : itm.venue.country}`}</span>
										<span>Time: {getDate(itm.datetime, 'h:mm a')} &nbsp;|&nbsp; <a target="_blank" rel="noopener noreferrer" href={itm.url}>Info</a></span>
									</div>
								</div>


							</div>


						)
					})
				}

			</div>
		</div>
	)
}

/**
 * NOTES
 *
 *


 id: "15297766"
url: "https://www.bandsintown.com/e/15297766?app_id=Test&came_from=267&utm_medium=api&utm_source=public_api&utm_campaign=event"
datetime: "2015-12-26T22:00:00"
title: ""
description: ""
venue:
country: "United States"
city: "Silver Spring"
latitude: "38.9914936"
name: "Station 1"
region: "MD"
longitude: "-77.0261462"
__proto__: Object
lineup: ["Naomi Achu Worldwide"]
offers: []
artist_id: "13261074"
on_sale_datetime: ""


 */